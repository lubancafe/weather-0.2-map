'use strict';

var forecastioWeather = ['$q', '$resource', '$http', 'FORECASTIO_KEY', 
  function($q, $resource, $http, FORECASTIO_KEY) {
  var url = 'https://api.forecast.io/forecast/' + FORECASTIO_KEY + '/';

  var weatherResource = $resource(url, {
    callback: 'JSON_CALLBACK',
  }, {
    get: {
      method: 'JSONP'
    }
  });

  return {
    //getAtLocation: function(lat, lng) {
    getCurrentWeather: function(lat, lng) {
      var deferred = $q.defer();;
      $http.jsonp(url + lat + ',' + lng + '?lang=zh&callback=JSON_CALLBACK').success(function(resp) {
        // fahrenheit => celsius
        var t = resp.currently.temperature;
        resp.currently.temperature = Math.round((t-32)/1.8);
        deferred.resolve(resp);
      });

      return deferred.promise; 
    }
  }
}];

angular.module('starter.services', ['ngResource'])
.factory('Cities', function() {
var cities = [
    { id: 0, name: '上海', lat:31.22, lgn:121.48 },
    { id: 1, name: '大苹果城' ,lat: 40.7127 , lgn: 74.0059 },
    { id: 2, name: '伦敦' ,lat:51.5072 , lgn: -0.09 },
    { id: 3, name: '失落的天使城' ,lat: 34.0500 , lgn: -118.2500 },
    { id: 4, name: '达拉斯' ,lat: 32.7758 , lgn:96.7967  },
    { id: 5, name: '法兰克福' ,lat:50.1117 , lgn: 8.6858 },
    { id: 6, name: '新德里' ,lat:28.6100 , lgn: 77.2300 },
    { id: 7, name: '迈阿密', lat:25.7877 , lgn: -80.2241 }
  ];

  return {
    all: function() {
      return cities;
    },
    get: function(cityId) {
      // Simple index lookup
      return cities[cityId];
    }
  }
})

.factory('DataStore', function() {
    //create datastore with default values
    var DataStore = {
        city:       '上海',
        latitude:   31.22,
        longitude:  121.48 };

    DataStore.setCity = function (value) {
        DataStore.city = value;
    };

    DataStore.setLatitude = function (value) {
        DataStore.latitude = value;
    };

    DataStore.setLongitude = function (value) {
        DataStore.longitude = value;
    };

    return DataStore;
})


.factory('Weather', forecastioWeather)

.service('GeoIP', function ($http, $q){

  this.getLocation = function() {
    var deferred = $q.defer();
    var geoData = {};

    $http.get('http://ip-api.com/json')
      .success(function(coordinates) {

        geoData.latitude = coordinates.lat;
        geoData.longitude = coordinates.lon;
        geoData.city = coordinates.city;

        deferred.resolve(geoData);
    });

    return deferred.promise; 
  };
})

;
