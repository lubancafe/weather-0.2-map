angular.module('starter.controllers', ['ionic', 'leaflet-directive'])
.constant('FORECASTIO_KEY', '785864172151072bf3ac70e2d5b730b3') // please apply your own forecast.io key

.controller('HomeCtrl', function($scope,$state,$ionicLoading,Weather,DataStore) {

  $scope.$on('$ionicView.enter', function(){
    //this is to fix ng-repeat slider width:0px;
    //read default settings into scope
    console.log('inside home');
    $scope.city  = DataStore.city;
    var latitude  =  DataStore.latitude;
    var longitude = DataStore.longitude;

    //call getCurrentWeather method in factory ‘Weather’
    Weather.getCurrentWeather(latitude,longitude).then(function(resp) {
      $scope.current = resp;
      console.log('GOT CURRENT', $scope.current);
      //debugger;
    }, function(error) {
      alert('Unable to get current conditions');
      console.error(error);
    });

  });

  $scope.doRefresh = function() {
    console.log('inside home doRefresh');
    $scope.city  = DataStore.city;
    var latitude  =  DataStore.latitude;
    var longitude = DataStore.longitude;

    $ionicLoading.show({
      template: 'Loading ...'
    });

    //Always bring me the latest Weather
    Weather.getCurrentWeather(latitude, longitude)
    .then(function(resp){

      $scope.current = resp;
      console.log('GOT CURRENT', $scope.current);

      $ionicLoading.hide();
      $scope.$broadcast('scroll.refreshComplete');
    }, function(error) {
      alert('Unable to get current conditions');
      console.error(error);
    });
  };

})

.controller('LocationsCtrl', function($scope,$state, Cities,DataStore) {
  $scope.cities = Cities.all();

  $scope.changeCity = function(cityId) {
    //get lat and longitude for seleted location
    var lat  = $scope.cities[cityId].lat; //latitude
    var lgn  = $scope.cities[cityId].lgn; //longitude
    var city = $scope.cities[cityId].name; //city name

    DataStore.setCity(city);
    DataStore.setLatitude(lat);
    DataStore.setLongitude(lgn);

    $state.go('tab.home');
  }
})

.controller('MapCtrl', function($scope, $location, leafletData, GeoIP,Weather,DataStore) {
    //manages map
    angular.extend($scope, {
        center: {
            lat: 31.22,
            lng: 121.48,
            zoom: 6
        },
        layers: {
          baselayers: {
              /*osm: {
                  name: 'OpenStreetMap',
                  url: 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
                  type: 'xyz'
              }, 
              bingAerial: {
                  name: 'Bing Aerial',
                  type: 'bing',
                  key: 'Aj6XtE1Q1rIvehmjn2Rh1LR2qvMGZ-8vPS9Hn3jCeUiToM77JFnf-kFRzyMELDol'
              }, */
              autoNavi: {
                  name: '高德',
                  url: 'https://webrd0{s}.is.autonavi.com/appmaptile?lang=zh_cn&size=1&scale=1&style=8&x={x}&y={y}&z={z}',
                  type: 'xyz',
                  layerOptions: {
                    subdomains: ["1", "2", "3", "4"],
                    attribution: '@高德地图'
                  }
              },

              bingRoad: {
                  name: 'Bing Road',
                  type: 'bing',
                  key: 'Aj6XtE1Q1rIvehmjn2Rh1LR2qvMGZ-8vPS9Hn3jCeUiToM77JFnf-kFRzyMELDol',
                  layerOptions: {
                      type: 'Road'
                  }
              },
              bingAerialWithLabels: {
                  name: 'Bing Aerial With Labels',
                  type: 'bing',
                  key: 'Aj6XtE1Q1rIvehmjn2Rh1LR2qvMGZ-8vPS9Hn3jCeUiToM77JFnf-kFRzyMELDol',
                  layerOptions: {
                      type: 'AerialWithLabels'
                  }
              }
          }
      },   
      events: {}     
    });

    $scope.markers = new Array();

    // on enter map view
    $scope.$on('$ionicView.enter', function(DataStore){
        //this is to fix ng-repeat slider width:0px;
        //read default settings into scope
        console.log('inside map');
        // get city, location by ip

        // get geolocation via current user ip
        GeoIP.getLocation().then(function(loc) {
            DataStore.city = loc.city;
            DataStore.latitude = loc.latitude;
            DataStore.longitude = loc.longitude;


            var latitude  =  loc.latitude;
            var longitude = loc.longitude;

            $scope.center.lat = latitude;
            $scope.center.lng = longitude;
            $scope.city = loc.city;
                

            //call getCurrentWeather method in factory ‘Weather’
            Weather.getCurrentWeather(latitude,longitude).then(function(resp) {
                $scope.current = resp;
                console.log('GOT CURRENT', $scope.current);
            //debugger;
            }, function(error) {
                alert('Unable to get current conditions');
                console.error(error);
            });

        }, function(error) {
            // error handler for GeoIP
        });

    });

    // show current temperature on click
    $scope.$on("leafletDirectiveMap.click", function(event, args){
        var leafEvent = args.leafletEvent;
        var pop_msg;

        console.log("MapEvent", leafEvent.latlng);

        //call getCurrentWeather method in factory ‘Weather’
        Weather.getCurrentWeather(leafEvent.latlng.lat,leafEvent.latlng.lng).then(function(resp) {

            pop_msg = '经纬度：' + leafEvent.latlng;
            pop_msg += '</br>现在天气：' + resp.currently.summary;
            pop_msg += '</br>温度是: ' + resp.currently.temperature + '° C';        
            // set city and latlng to DataStore

            $scope.markers.push({
                lat: leafEvent.latlng.lat,
                lng: leafEvent.latlng.lng,
                focus: true,
                message: pop_msg,
                label: {
                            message: "点我",
                            options: {
                                noHide: true
                            }
                        }
            });
        //debugger;
        }, function(error) {
            alert('Unable to get current conditions');
            console.error(error);
        });

    });

    $scope.$on("leafletDirectiveMap.contextmenu", function(e, args) {
        var leafEvent = args.leafletEvent;
        console.log("MapEvent", leafEvent.latlng);

        var latlng = leafEvent.latlng;

        //call getCurrentWeather method in factory ‘Weather’
        Weather.getCurrentWeather($scope.center.lat,$scope.center.lng).then(function(resp) {
            $scope.current = resp;
            console.log('GOT CURRENT', $scope.current);
            var pop_msg;
            pop_msg = '经纬度：' + latlng;
            pop_msg += '</br>现在的温度是: ' + $scope.current.currently.temperature;        
            // set city and latlng to DataStore

            var popup = L.popup()
                .setLatLng(latlng)
                .setContent(pop_msg);
            leafletData.getMap().then(function(map) {
            map.openPopup(popup);
            });
            
        //debugger;
        }, function(error) {
            alert('Unable to get current conditions');
            console.error(error);
        });
        

    });

    $scope.$on("centerUrlHash", function(event, centerHash) {
        console.log("url", centerHash);
        $location.search({ c: centerHash });
    });

    $scope.$on('click', function(event) {
      alert(event.latlng);
      console.log("onclick", event.latlng);
    });    
    $scope.changeLocation = function(centerHash) {
        $scope.center.lat = 31.22;
        $scope.center.lng = 121.48;
        $scope.center.zoom = 6;
    };
    
})

;